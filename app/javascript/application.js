// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "@hotwired/turbo-rails"
import "controllers"
import * as bootstrap from "bootstrap"

$(document).ready(function(){
    $('#telefone').inputmask('(99) 99999-9999');
    $('#data').inputmask('99/99/9999');
    $('#cpf').inputmask('999.999.999-99');
    $("#currency").maskMoney({prefix:'R$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false});
    $('.money').mask('000.000.000.000.000,00', {reverse: true});
    $('.money2').mask("#.##0,00", {reverse: true});
})
