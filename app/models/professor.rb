class Professor < ApplicationRecord
  belongs_to :tipo_professor
  belongs_to :laboratorio

  has_many :projeto_professors
  has_many :projetos, through: :projeto_professors
  accepts_nested_attributes_for :projetos
end
