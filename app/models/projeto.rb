class Projeto < ApplicationRecord
  belongs_to :area_conhecimento

  has_many :projeto_professors
  has_many :projeto_alunos

  accepts_nested_attributes_for :projeto_alunos, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :projeto_professors, reject_if: :all_blank, allow_destroy: true

  has_many :alunos, through: :projeto_alunos
  accepts_nested_attributes_for :alunos

  has_many :professors, through: :projeto_professors
  accepts_nested_attributes_for :professors
end
