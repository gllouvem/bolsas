class Laboratorio < ApplicationRecord
  belongs_to :centro

  #validates_associated :centro, :message => " O centro deve existir !"

  validates  :nome, :presence => { :message => " é um campo obrigatório !"}
  validates  :sigla, :presence => { :message => " é um campo obrigatório !"}
  validates  :centro, :presence => { :message => " é um campo obrigatório !"}
end
