class Aluno < ApplicationRecord
  belongs_to :curso
  belongs_to :cota

  has_many :documento_alunos
  accepts_nested_attributes_for :documento_alunos, reject_if: :all_blank, allow_destroy: true

  has_many :projeto_alunos
  has_many :projetos, through: :projeto_alunos
  accepts_nested_attributes_for :projetos


end
