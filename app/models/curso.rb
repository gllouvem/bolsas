class Curso < ApplicationRecord
  belongs_to :centro
  belongs_to :instituicao

  validates  :nome, :sigla, :presence => { :message => " é um campo obrigatório !"}
end
