class DocumentoAluno < ApplicationRecord
  belongs_to :aluno
  belongs_to :tipo_documento

  has_one_attached :documento
end
