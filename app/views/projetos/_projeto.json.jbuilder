json.extract! projeto, :id, :titulo, :inicio, :fim, :palavra_chave, :area_conhecimento_id, :created_at, :updated_at
json.url projeto_url(projeto, format: :json)
