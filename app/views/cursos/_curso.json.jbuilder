json.extract! curso, :id, :nome, :centro_id, :instituicao_id, :sigla, :created_at, :updated_at
json.url curso_url(curso, format: :json)
