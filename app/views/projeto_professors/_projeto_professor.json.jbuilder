json.extract! projeto_professor, :id, :projeto_id, :professor_id, :inicio, :fim, :created_at, :updated_at
json.url projeto_professor_url(projeto_professor, format: :json)
