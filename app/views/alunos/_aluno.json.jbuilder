json.extract! aluno, :id, :matricula, :nome, :cpf, :curso_id, :cota_id, :created_at, :updated_at
json.url aluno_url(aluno, format: :json)
