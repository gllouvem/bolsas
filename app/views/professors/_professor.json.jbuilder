json.extract! professor, :id, :matricula, :nome, :cpf, :tipo_professor_id, :laboratorio_id, :created_at, :updated_at
json.url professor_url(professor, format: :json)
