json.extract! documento_aluno, :id, :aluno_id, :tipo_documento_id, :documento, :created_at, :updated_at
json.url documento_aluno_url(documento_aluno, format: :json)
