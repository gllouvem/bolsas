json.extract! laboratorio, :id, :nome, :sigla, :centro_id, :created_at, :updated_at
json.url laboratorio_url(laboratorio, format: :json)
