json.extract! tipo_professor, :id, :descricao, :created_at, :updated_at
json.url tipo_professor_url(tipo_professor, format: :json)
