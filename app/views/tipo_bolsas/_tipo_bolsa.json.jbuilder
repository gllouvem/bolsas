json.extract! tipo_bolsa, :id, :nome, :valor, :created_at, :updated_at
json.url tipo_bolsa_url(tipo_bolsa, format: :json)
