json.extract! centro, :id, :nome, :sigla, :created_at, :updated_at
json.url centro_url(centro, format: :json)
