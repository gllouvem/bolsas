json.extract! projeto_aluno, :id, :projeto_id, :aluno_id, :inicio, :fim, :plano_trabalho, :tipo_bolsa_id, :created_at, :updated_at
json.url projeto_aluno_url(projeto_aluno, format: :json)
