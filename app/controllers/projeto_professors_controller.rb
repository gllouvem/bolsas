class ProjetoProfessorsController < ApplicationController
  before_action :set_projeto_professor, only: %i[ show edit update destroy ]

  # GET /projeto_professors or /projeto_professors.json
  def index
    @projeto_professors = ProjetoProfessor.all
  end

  # GET /projeto_professors/1 or /projeto_professors/1.json
  def show
  end

  # GET /projeto_professors/new
  def new
    @projeto_professor = ProjetoProfessor.new
  end

  # GET /projeto_professors/1/edit
  def edit
  end

  # POST /projeto_professors or /projeto_professors.json
  def create
    @projeto_professor = ProjetoProfessor.new(projeto_professor_params)

    respond_to do |format|
      if @projeto_professor.save
        format.html { redirect_to @projeto_professor, notice: "Projeto professor was successfully created." }
        format.json { render :show, status: :created, location: @projeto_professor }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @projeto_professor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projeto_professors/1 or /projeto_professors/1.json
  def update
    respond_to do |format|
      if @projeto_professor.update(projeto_professor_params)
        format.html { redirect_to @projeto_professor, notice: "Projeto professor was successfully updated." }
        format.json { render :show, status: :ok, location: @projeto_professor }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @projeto_professor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projeto_professors/1 or /projeto_professors/1.json
  def destroy
    @projeto_professor.destroy!

    respond_to do |format|
      format.html { redirect_to projeto_professors_path, status: :see_other, notice: "Projeto professor was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_projeto_professor
      @projeto_professor = ProjetoProfessor.find(params.expect(:id))
    end

    # Only allow a list of trusted parameters through.
    def projeto_professor_params
      params.expect(projeto_professor: [ :projeto_id, :professor_id, :inicio, :fim ])
    end
end
