class DocumentoAlunosController < ApplicationController
  before_action :set_documento_aluno, only: %i[ show edit update destroy ]

  # GET /documento_alunos or /documento_alunos.json
  def index
    @documento_alunos = DocumentoAluno.all
  end

  # GET /documento_alunos/1 or /documento_alunos/1.json
  def show
  end

  # GET /documento_alunos/new
  def new
    @documento_aluno = DocumentoAluno.new
  end

  # GET /documento_alunos/1/edit
  def edit
  end

  # POST /documento_alunos or /documento_alunos.json
  def create
    @documento_aluno = DocumentoAluno.new(documento_aluno_params)

    respond_to do |format|
      if @documento_aluno.save
        format.html { redirect_to @documento_aluno, notice: "Documento aluno was successfully created." }
        format.json { render :show, status: :created, location: @documento_aluno }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @documento_aluno.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /documento_alunos/1 or /documento_alunos/1.json
  def update
    respond_to do |format|
      if @documento_aluno.update(documento_aluno_params)
        format.html { redirect_to @documento_aluno, notice: "Documento aluno was successfully updated." }
        format.json { render :show, status: :ok, location: @documento_aluno }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @documento_aluno.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documento_alunos/1 or /documento_alunos/1.json
  def destroy
    @documento_aluno.destroy!

    respond_to do |format|
      format.html { redirect_to documento_alunos_path, status: :see_other, notice: "Documento aluno was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_documento_aluno
      @documento_aluno = DocumentoAluno.find(params.expect(:id))
    end

    # Only allow a list of trusted parameters through.
    def documento_aluno_params
      params.expect(documento_aluno: [ :aluno_id, :tipo_documento_id, :documento ])
    end
end
