class ProjetoAlunosController < ApplicationController
  before_action :set_projeto_aluno, only: %i[ show edit update destroy ]

  # GET /projeto_alunos or /projeto_alunos.json
  def index
    @projeto_alunos = ProjetoAluno.all
  end

  # GET /projeto_alunos/1 or /projeto_alunos/1.json
  def show
  end

  # GET /projeto_alunos/new
  def new
    @projeto_aluno = ProjetoAluno.new
  end

  # GET /projeto_alunos/1/edit
  def edit
  end

  # POST /projeto_alunos or /projeto_alunos.json
  def create
    @projeto_aluno = ProjetoAluno.new(projeto_aluno_params)

    respond_to do |format|
      if @projeto_aluno.save
        format.html { redirect_to @projeto_aluno, notice: "Projeto aluno was successfully created." }
        format.json { render :show, status: :created, location: @projeto_aluno }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @projeto_aluno.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projeto_alunos/1 or /projeto_alunos/1.json
  def update
    respond_to do |format|
      if @projeto_aluno.update(projeto_aluno_params)
        format.html { redirect_to @projeto_aluno, notice: "Projeto aluno was successfully updated." }
        format.json { render :show, status: :ok, location: @projeto_aluno }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @projeto_aluno.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projeto_alunos/1 or /projeto_alunos/1.json
  def destroy
    @projeto_aluno.destroy!

    respond_to do |format|
      format.html { redirect_to projeto_alunos_path, status: :see_other, notice: "Projeto aluno was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_projeto_aluno
      @projeto_aluno = ProjetoAluno.find(params.expect(:id))
    end

    # Only allow a list of trusted parameters through.
    def projeto_aluno_params
      params.expect(projeto_aluno: [ :projeto_id, :aluno_id, :inicio, :fim, :plano_trabalho, :tipo_bolsa_id ])
    end
end
