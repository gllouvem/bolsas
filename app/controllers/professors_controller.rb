class ProfessorsController < ApplicationController
  before_action :set_professor, only: %i[ show edit update destroy ]

  # GET /professors or /professors.json
  def index
    if params[:nome]
      @professors = Professor.where("lower(unaccent(nome)) like lower(unaccent( ? ))", "%" + params[:nome].to_s + "%").order(:nome).page(params[:page])
    end
  end

  # GET /professors/1 or /professors/1.json
  def show
  end

  # GET /professors/new
  def new
    @professor = Professor.new
  end

  # GET /professors/1/edit
  def edit
  end

  # POST /professors or /professors.json
  def create
    @professor = Professor.new(professor_params)

    respond_to do |format|
      if @professor.save
        format.html { redirect_to @professor, notice: "Professor criado com sucesso." }
        format.json { render :show, status: :created, location: @professor }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @professor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /professors/1 or /professors/1.json
  def update
    respond_to do |format|
      if @professor.update(professor_params)
        format.html { redirect_to @professor, notice: "Professor salvo com sucesso." }
        format.json { render :show, status: :ok, location: @professor }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @professor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /professors/1 or /professors/1.json
  def destroy
    @professor.destroy!

    respond_to do |format|
      format.html { redirect_to professors_path, status: :see_other, notice: "Professor was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_professor
      @professor = Professor.find(params.expect(:id))
    end

    # Only allow a list of trusted parameters through.
    def professor_params
      params.expect(professor: [ :matricula, :nome, :cpf, :tipo_professor_id, :laboratorio_id ])
    end
end
