class CotasController < ApplicationController
  before_action :set_cotum, only: %i[ show edit update destroy ]

  # GET /cota or /cota.json
  def index
    @cotas = Cota.all
  end

  # GET /cota/1 or /cota/1.json
  def show
  end

  # GET /cota/new
  def new
    @cota = Cota.new
  end

  # GET /cota/1/edit
  def edit
  end

  # POST /cota or /cota.json
  def create
    @cota = Cota.new(cota_params)

    respond_to do |format|
      if @cota.save
        format.html { redirect_to cotas_path, notice: "Cota criada com sucesso." }
        format.json { render :show, status: :created, location: @cota }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @cota.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cota/1 or /cota/1.json
  def update
    respond_to do |format|
      if @cota.update(cota_params)
        format.html { redirect_to cotas_path, notice: "Cota salva com sucesso." }
        format.json { render :show, status: :ok, location: @cota }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @cota.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cota/1 or /cota/1.json
  def destroy
    @cota.destroy!

    respond_to do |format|
      format.html { redirect_to cotas_path, status: :see_other, notice: "Cotum was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cotum
      @cota = Cota.find(params.expect(:id))
    end

    # Only allow a list of trusted parameters through.
    def cota_params
      params.expect(cota: [ :nome ])
    end
end
