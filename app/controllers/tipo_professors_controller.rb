class TipoProfessorsController < ApplicationController
  before_action :set_tipo_professor, only: %i[ show edit update destroy ]

  # GET /tipo_professors or /tipo_professors.json
  def index
    @tipo_professors = TipoProfessor.all
  end

  # GET /tipo_professors/1 or /tipo_professors/1.json
  def show
  end

  # GET /tipo_professors/new
  def new
    @tipo_professor = TipoProfessor.new
  end

  # GET /tipo_professors/1/edit
  def edit
  end

  # POST /tipo_professors or /tipo_professors.json
  def create
    @tipo_professor = TipoProfessor.new(tipo_professor_params)

    respond_to do |format|
      if @tipo_professor.save
        format.html { redirect_to tipo_professors_path, notice: "Tipo professor criado com sucesso." }
        format.json { render :show, status: :created, location: @tipo_professor }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @tipo_professor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tipo_professors/1 or /tipo_professors/1.json
  def update
    respond_to do |format|
      if @tipo_professor.update(tipo_professor_params)
        format.html { redirect_to tipo_professors_path, notice: "Tipo professor salvo com sucesso." }
        format.json { render :show, status: :ok, location: @tipo_professor }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: tipo_professors_path.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tipo_professors/1 or /tipo_professors/1.json
  def destroy
    @tipo_professor.destroy!

    respond_to do |format|
      format.html { redirect_to tipo_professors_path, status: :see_other, notice: "Tipo professor excluido com sucesso." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tipo_professor
      @tipo_professor = TipoProfessor.find(params.expect(:id))
    end

    # Only allow a list of trusted parameters through.
    def tipo_professor_params
      params.expect(tipo_professor: [ :descricao ])
    end
end
