class TipoBolsasController < ApplicationController
  before_action :set_tipo_bolsa, only: %i[ show edit update destroy ]

  # GET /tipo_bolsas or /tipo_bolsas.json
  def index
    @tipo_bolsas = TipoBolsa.all
  end

  # GET /tipo_bolsas/1 or /tipo_bolsas/1.json
  def show
  end

  # GET /tipo_bolsas/new
  def new
    @tipo_bolsa = TipoBolsa.new
  end

  # GET /tipo_bolsas/1/edit
  def edit
  end

  # POST /tipo_bolsas or /tipo_bolsas.json
  def create
    @tipo_bolsa = TipoBolsa.new(tipo_bolsa_params)

    respond_to do |format|
      if @tipo_bolsa.save
        format.html { redirect_to tipo_bolsas_path, notice: "Tipo bolsa criado com sucesso." }
        format.json { render :show, status: :created, location: @tipo_bolsa }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @tipo_bolsa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tipo_bolsas/1 or /tipo_bolsas/1.json
  def update
    respond_to do |format|
      if @tipo_bolsa.update(tipo_bolsa_params)
        format.html { redirect_to tipo_bolsas_path, notice: "Tipo bolsa salvo com sucesso." }
        format.json { render :show, status: :ok, location: @tipo_bolsa }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @tipo_bolsa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tipo_bolsas/1 or /tipo_bolsas/1.json
  def destroy
    @tipo_bolsa.destroy!

    respond_to do |format|
      format.html { redirect_to tipo_bolsas_path, status: :see_other, notice: "Tipo bolsa was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tipo_bolsa
      @tipo_bolsa = TipoBolsa.find(params.expect(:id))
    end

    # Only allow a list of trusted parameters through.
    def tipo_bolsa_params
      params.expect(tipo_bolsa: [ :nome, :valor ])
    end
end
