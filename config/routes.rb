Rails.application.routes.draw do
  resources :documento_alunos
  resources :tipo_documentos
  resources :projeto_alunos
  resources :projeto_professors
  resources :tipo_bolsas
  resources :alunos
  resources :cotas
  resources :projetos
  resources :area_conhecimentos
  resources :professors
  resources :cursos
  resources :instituicaos
  resources :laboratorios
  resources :centros
  resources :tipo_professors
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # Render dynamic PWA files from app/views/pwa/* (remember to link manifest in application.html.erb)
  # get "manifest" => "rails/pwa#manifest", as: :pwa_manifest
  # get "service-worker" => "rails/pwa#service_worker", as: :pwa_service_worker

  # Defines the root path route ("/")
  root "application#index"
end
