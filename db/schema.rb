# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[8.0].define(version: 2024_11_29_182044) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_catalog.plpgsql"
  enable_extension "unaccent"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum"
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "alunos", force: :cascade do |t|
    t.string "matricula"
    t.string "nome"
    t.string "cpf"
    t.bigint "curso_id", null: false
    t.bigint "cota_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cota_id"], name: "index_alunos_on_cota_id"
    t.index ["curso_id"], name: "index_alunos_on_curso_id"
  end

  create_table "area_conhecimentos", force: :cascade do |t|
    t.string "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "centros", force: :cascade do |t|
    t.string "nome"
    t.string "sigla"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cotas", force: :cascade do |t|
    t.string "nome"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cursos", force: :cascade do |t|
    t.string "nome"
    t.bigint "centro_id", null: false
    t.bigint "instituicao_id", null: false
    t.string "sigla"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["centro_id"], name: "index_cursos_on_centro_id"
    t.index ["instituicao_id"], name: "index_cursos_on_instituicao_id"
  end

  create_table "documento_alunos", force: :cascade do |t|
    t.bigint "aluno_id", null: false
    t.bigint "tipo_documento_id", null: false
    t.string "documento"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["aluno_id"], name: "index_documento_alunos_on_aluno_id"
    t.index ["tipo_documento_id"], name: "index_documento_alunos_on_tipo_documento_id"
  end

  create_table "instituicaos", force: :cascade do |t|
    t.string "nome"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "laboratorios", force: :cascade do |t|
    t.string "nome"
    t.string "sigla"
    t.bigint "centro_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["centro_id"], name: "index_laboratorios_on_centro_id"
  end

  create_table "professors", force: :cascade do |t|
    t.string "matricula"
    t.string "nome"
    t.string "cpf"
    t.bigint "tipo_professor_id", null: false
    t.bigint "laboratorio_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["laboratorio_id"], name: "index_professors_on_laboratorio_id"
    t.index ["tipo_professor_id"], name: "index_professors_on_tipo_professor_id"
  end

  create_table "projeto_alunos", force: :cascade do |t|
    t.bigint "projeto_id", null: false
    t.bigint "aluno_id", null: false
    t.date "inicio"
    t.date "fim"
    t.string "plano_trabalho"
    t.bigint "tipo_bolsa_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["aluno_id"], name: "index_projeto_alunos_on_aluno_id"
    t.index ["projeto_id"], name: "index_projeto_alunos_on_projeto_id"
    t.index ["tipo_bolsa_id"], name: "index_projeto_alunos_on_tipo_bolsa_id"
  end

  create_table "projeto_professors", force: :cascade do |t|
    t.bigint "projeto_id", null: false
    t.bigint "professor_id", null: false
    t.date "inicio"
    t.date "fim"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["professor_id"], name: "index_projeto_professors_on_professor_id"
    t.index ["projeto_id"], name: "index_projeto_professors_on_projeto_id"
  end

  create_table "projetos", force: :cascade do |t|
    t.string "titulo"
    t.date "inicio"
    t.date "fim"
    t.string "palavra_chave"
    t.bigint "area_conhecimento_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["area_conhecimento_id"], name: "index_projetos_on_area_conhecimento_id"
  end

  create_table "tipo_bolsas", force: :cascade do |t|
    t.string "nome"
    t.float "valor"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipo_documentos", force: :cascade do |t|
    t.string "nome"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tipo_professors", force: :cascade do |t|
    t.string "descricao"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "alunos", "cotas"
  add_foreign_key "alunos", "cursos"
  add_foreign_key "cursos", "centros"
  add_foreign_key "cursos", "instituicaos"
  add_foreign_key "documento_alunos", "alunos"
  add_foreign_key "documento_alunos", "tipo_documentos"
  add_foreign_key "laboratorios", "centros"
  add_foreign_key "professors", "laboratorios"
  add_foreign_key "professors", "tipo_professors"
  add_foreign_key "projeto_alunos", "alunos"
  add_foreign_key "projeto_alunos", "projetos"
  add_foreign_key "projeto_alunos", "tipo_bolsas"
  add_foreign_key "projeto_professors", "professors"
  add_foreign_key "projeto_professors", "projetos"
  add_foreign_key "projetos", "area_conhecimentos"
end
