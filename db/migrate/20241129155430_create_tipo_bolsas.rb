class CreateTipoBolsas < ActiveRecord::Migration[8.0]
  def change
    create_table :tipo_bolsas do |t|
      t.string :nome
      t.float :valor

      t.timestamps
    end
  end
end
