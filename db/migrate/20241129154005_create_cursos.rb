class CreateCursos < ActiveRecord::Migration[8.0]
  def change
    create_table :cursos do |t|
      t.string :nome
      t.belongs_to :centro, null: false, foreign_key: true
      t.belongs_to :instituicao, null: false, foreign_key: true
      t.string :sigla

      t.timestamps
    end
  end
end
