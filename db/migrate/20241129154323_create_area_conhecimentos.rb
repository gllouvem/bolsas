class CreateAreaConhecimentos < ActiveRecord::Migration[8.0]
  def change
    create_table :area_conhecimentos do |t|
      t.string :descricao

      t.timestamps
    end
  end
end
