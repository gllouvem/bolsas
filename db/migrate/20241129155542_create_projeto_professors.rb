class CreateProjetoProfessors < ActiveRecord::Migration[8.0]
  def change
    create_table :projeto_professors do |t|
      t.belongs_to :projeto, null: false, foreign_key: true
      t.belongs_to :professor, null: false, foreign_key: true
      t.date :inicio
      t.date :fim

      t.timestamps
    end
  end
end
