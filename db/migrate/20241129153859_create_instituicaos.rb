class CreateInstituicaos < ActiveRecord::Migration[8.0]
  def change
    create_table :instituicaos do |t|
      t.string :nome

      t.timestamps
    end
  end
end
