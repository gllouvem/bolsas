class CreateDocumentoAlunos < ActiveRecord::Migration[8.0]
  def change
    create_table :documento_alunos do |t|
      t.belongs_to :aluno, null: false, foreign_key: true
      t.belongs_to :tipo_documento, null: false, foreign_key: true
      t.string :documento

      t.timestamps
    end
  end
end
