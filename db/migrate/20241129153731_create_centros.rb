class CreateCentros < ActiveRecord::Migration[8.0]
  def change
    create_table :centros do |t|
      t.string :nome
      t.string :sigla

      t.timestamps
    end
  end
end
