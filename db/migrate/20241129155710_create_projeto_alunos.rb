class CreateProjetoAlunos < ActiveRecord::Migration[8.0]
  def change
    create_table :projeto_alunos do |t|
      t.belongs_to :projeto, null: false, foreign_key: true
      t.belongs_to :aluno, null: false, foreign_key: true
      t.date :inicio
      t.date :fim
      t.string :plano_trabalho
      t.belongs_to :tipo_bolsa, null: false, foreign_key: true

      t.timestamps
    end
  end
end
