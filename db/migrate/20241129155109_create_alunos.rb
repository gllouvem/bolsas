class CreateAlunos < ActiveRecord::Migration[8.0]
  def change
    create_table :alunos do |t|
      t.string :matricula
      t.string :nome
      t.string :cpf
      t.belongs_to :curso, null: false, foreign_key: true
      t.belongs_to :cota, null: false, foreign_key: true

      t.timestamps
    end
  end
end
