class CreateProfessors < ActiveRecord::Migration[8.0]
  def change
    create_table :professors do |t|
      t.string :matricula
      t.string :nome
      t.string :cpf
      t.belongs_to :tipo_professor, null: false, foreign_key: true
      t.belongs_to :laboratorio, null: false, foreign_key: true

      t.timestamps
    end
  end
end
