class CreateLaboratorios < ActiveRecord::Migration[8.0]
  def change
    create_table :laboratorios do |t|
      t.string :nome
      t.string :sigla
      t.belongs_to :centro, null: false, foreign_key: true

      t.timestamps
    end
  end
end
