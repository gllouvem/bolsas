class CreateProjetos < ActiveRecord::Migration[8.0]
  def change
    create_table :projetos do |t|
      t.string :titulo
      t.date :inicio
      t.date :fim
      t.string :palavra_chave
      t.belongs_to :area_conhecimento, null: false, foreign_key: true

      t.timestamps
    end
  end
end
