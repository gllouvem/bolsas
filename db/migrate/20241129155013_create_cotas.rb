class CreateCotas < ActiveRecord::Migration[8.0]
  def change
    create_table :cotas do |t|
      t.string :nome

      t.timestamps
    end
  end
end
