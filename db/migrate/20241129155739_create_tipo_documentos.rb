class CreateTipoDocumentos < ActiveRecord::Migration[8.0]
  def change
    create_table :tipo_documentos do |t|
      t.string :nome

      t.timestamps
    end
  end
end
