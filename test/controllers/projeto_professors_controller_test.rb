require "test_helper"

class ProjetoProfessorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @projeto_professor = projeto_professors(:one)
  end

  test "should get index" do
    get projeto_professors_url
    assert_response :success
  end

  test "should get new" do
    get new_projeto_professor_url
    assert_response :success
  end

  test "should create projeto_professor" do
    assert_difference("ProjetoProfessor.count") do
      post projeto_professors_url, params: { projeto_professor: { fim: @projeto_professor.fim, inicio: @projeto_professor.inicio, professor_id: @projeto_professor.professor_id, projeto_id: @projeto_professor.projeto_id } }
    end

    assert_redirected_to projeto_professor_url(ProjetoProfessor.last)
  end

  test "should show projeto_professor" do
    get projeto_professor_url(@projeto_professor)
    assert_response :success
  end

  test "should get edit" do
    get edit_projeto_professor_url(@projeto_professor)
    assert_response :success
  end

  test "should update projeto_professor" do
    patch projeto_professor_url(@projeto_professor), params: { projeto_professor: { fim: @projeto_professor.fim, inicio: @projeto_professor.inicio, professor_id: @projeto_professor.professor_id, projeto_id: @projeto_professor.projeto_id } }
    assert_redirected_to projeto_professor_url(@projeto_professor)
  end

  test "should destroy projeto_professor" do
    assert_difference("ProjetoProfessor.count", -1) do
      delete projeto_professor_url(@projeto_professor)
    end

    assert_redirected_to projeto_professors_url
  end
end
