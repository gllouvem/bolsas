require "test_helper"

class CotaControllerTest < ActionDispatch::IntegrationTest
  setup do
    @cotum = cota(:one)
  end

  test "should get index" do
    get cota_url
    assert_response :success
  end

  test "should get new" do
    get new_cotum_url
    assert_response :success
  end

  test "should create cotum" do
    assert_difference("Cotum.count") do
      post cota_url, params: { cotum: { nome: @cotum.nome } }
    end

    assert_redirected_to cotum_url(Cotum.last)
  end

  test "should show cotum" do
    get cotum_url(@cotum)
    assert_response :success
  end

  test "should get edit" do
    get edit_cotum_url(@cotum)
    assert_response :success
  end

  test "should update cotum" do
    patch cotum_url(@cotum), params: { cotum: { nome: @cotum.nome } }
    assert_redirected_to cotum_url(@cotum)
  end

  test "should destroy cotum" do
    assert_difference("Cotum.count", -1) do
      delete cotum_url(@cotum)
    end

    assert_redirected_to cota_url
  end
end
