require "test_helper"

class ProjetoAlunosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @projeto_aluno = projeto_alunos(:one)
  end

  test "should get index" do
    get projeto_alunos_url
    assert_response :success
  end

  test "should get new" do
    get new_projeto_aluno_url
    assert_response :success
  end

  test "should create projeto_aluno" do
    assert_difference("ProjetoAluno.count") do
      post projeto_alunos_url, params: { projeto_aluno: { aluno_id: @projeto_aluno.aluno_id, fim: @projeto_aluno.fim, inicio: @projeto_aluno.inicio, plano_trabalho: @projeto_aluno.plano_trabalho, projeto_id: @projeto_aluno.projeto_id, tipo_bolsa_id: @projeto_aluno.tipo_bolsa_id } }
    end

    assert_redirected_to projeto_aluno_url(ProjetoAluno.last)
  end

  test "should show projeto_aluno" do
    get projeto_aluno_url(@projeto_aluno)
    assert_response :success
  end

  test "should get edit" do
    get edit_projeto_aluno_url(@projeto_aluno)
    assert_response :success
  end

  test "should update projeto_aluno" do
    patch projeto_aluno_url(@projeto_aluno), params: { projeto_aluno: { aluno_id: @projeto_aluno.aluno_id, fim: @projeto_aluno.fim, inicio: @projeto_aluno.inicio, plano_trabalho: @projeto_aluno.plano_trabalho, projeto_id: @projeto_aluno.projeto_id, tipo_bolsa_id: @projeto_aluno.tipo_bolsa_id } }
    assert_redirected_to projeto_aluno_url(@projeto_aluno)
  end

  test "should destroy projeto_aluno" do
    assert_difference("ProjetoAluno.count", -1) do
      delete projeto_aluno_url(@projeto_aluno)
    end

    assert_redirected_to projeto_alunos_url
  end
end
