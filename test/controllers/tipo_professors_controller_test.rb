require "test_helper"

class TipoProfessorsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipo_professor = tipo_professors(:one)
  end

  test "should get index" do
    get tipo_professors_url
    assert_response :success
  end

  test "should get new" do
    get new_tipo_professor_url
    assert_response :success
  end

  test "should create tipo_professor" do
    assert_difference("TipoProfessor.count") do
      post tipo_professors_url, params: { tipo_professor: { descricao: @tipo_professor.descricao } }
    end

    assert_redirected_to tipo_professor_url(TipoProfessor.last)
  end

  test "should show tipo_professor" do
    get tipo_professor_url(@tipo_professor)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipo_professor_url(@tipo_professor)
    assert_response :success
  end

  test "should update tipo_professor" do
    patch tipo_professor_url(@tipo_professor), params: { tipo_professor: { descricao: @tipo_professor.descricao } }
    assert_redirected_to tipo_professor_url(@tipo_professor)
  end

  test "should destroy tipo_professor" do
    assert_difference("TipoProfessor.count", -1) do
      delete tipo_professor_url(@tipo_professor)
    end

    assert_redirected_to tipo_professors_url
  end
end
