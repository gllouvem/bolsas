require "test_helper"

class TipoBolsasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipo_bolsa = tipo_bolsas(:one)
  end

  test "should get index" do
    get tipo_bolsas_url
    assert_response :success
  end

  test "should get new" do
    get new_tipo_bolsa_url
    assert_response :success
  end

  test "should create tipo_bolsa" do
    assert_difference("TipoBolsa.count") do
      post tipo_bolsas_url, params: { tipo_bolsa: { nome: @tipo_bolsa.nome, valor: @tipo_bolsa.valor } }
    end

    assert_redirected_to tipo_bolsa_url(TipoBolsa.last)
  end

  test "should show tipo_bolsa" do
    get tipo_bolsa_url(@tipo_bolsa)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipo_bolsa_url(@tipo_bolsa)
    assert_response :success
  end

  test "should update tipo_bolsa" do
    patch tipo_bolsa_url(@tipo_bolsa), params: { tipo_bolsa: { nome: @tipo_bolsa.nome, valor: @tipo_bolsa.valor } }
    assert_redirected_to tipo_bolsa_url(@tipo_bolsa)
  end

  test "should destroy tipo_bolsa" do
    assert_difference("TipoBolsa.count", -1) do
      delete tipo_bolsa_url(@tipo_bolsa)
    end

    assert_redirected_to tipo_bolsas_url
  end
end
