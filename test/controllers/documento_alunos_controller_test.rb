require "test_helper"

class DocumentoAlunosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @documento_aluno = documento_alunos(:one)
  end

  test "should get index" do
    get documento_alunos_url
    assert_response :success
  end

  test "should get new" do
    get new_documento_aluno_url
    assert_response :success
  end

  test "should create documento_aluno" do
    assert_difference("DocumentoAluno.count") do
      post documento_alunos_url, params: { documento_aluno: { aluno_id: @documento_aluno.aluno_id, documento: @documento_aluno.documento, tipo_documento_id: @documento_aluno.tipo_documento_id } }
    end

    assert_redirected_to documento_aluno_url(DocumentoAluno.last)
  end

  test "should show documento_aluno" do
    get documento_aluno_url(@documento_aluno)
    assert_response :success
  end

  test "should get edit" do
    get edit_documento_aluno_url(@documento_aluno)
    assert_response :success
  end

  test "should update documento_aluno" do
    patch documento_aluno_url(@documento_aluno), params: { documento_aluno: { aluno_id: @documento_aluno.aluno_id, documento: @documento_aluno.documento, tipo_documento_id: @documento_aluno.tipo_documento_id } }
    assert_redirected_to documento_aluno_url(@documento_aluno)
  end

  test "should destroy documento_aluno" do
    assert_difference("DocumentoAluno.count", -1) do
      delete documento_aluno_url(@documento_aluno)
    end

    assert_redirected_to documento_alunos_url
  end
end
