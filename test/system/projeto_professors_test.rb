require "application_system_test_case"

class ProjetoProfessorsTest < ApplicationSystemTestCase
  setup do
    @projeto_professor = projeto_professors(:one)
  end

  test "visiting the index" do
    visit projeto_professors_url
    assert_selector "h1", text: "Projeto professors"
  end

  test "should create projeto professor" do
    visit projeto_professors_url
    click_on "New projeto professor"

    fill_in "Fim", with: @projeto_professor.fim
    fill_in "Inicio", with: @projeto_professor.inicio
    fill_in "Professor", with: @projeto_professor.professor_id
    fill_in "Projeto", with: @projeto_professor.projeto_id
    click_on "Create Projeto professor"

    assert_text "Projeto professor was successfully created"
    click_on "Back"
  end

  test "should update Projeto professor" do
    visit projeto_professor_url(@projeto_professor)
    click_on "Edit this projeto professor", match: :first

    fill_in "Fim", with: @projeto_professor.fim
    fill_in "Inicio", with: @projeto_professor.inicio
    fill_in "Professor", with: @projeto_professor.professor_id
    fill_in "Projeto", with: @projeto_professor.projeto_id
    click_on "Update Projeto professor"

    assert_text "Projeto professor was successfully updated"
    click_on "Back"
  end

  test "should destroy Projeto professor" do
    visit projeto_professor_url(@projeto_professor)
    click_on "Destroy this projeto professor", match: :first

    assert_text "Projeto professor was successfully destroyed"
  end
end
