require "application_system_test_case"

class CotaTest < ApplicationSystemTestCase
  setup do
    @cotum = cota(:one)
  end

  test "visiting the index" do
    visit cota_url
    assert_selector "h1", text: "Cota"
  end

  test "should create cotum" do
    visit cota_url
    click_on "New cotum"

    fill_in "Nome", with: @cotum.nome
    click_on "Create Cotum"

    assert_text "Cotum was successfully created"
    click_on "Back"
  end

  test "should update Cotum" do
    visit cotum_url(@cotum)
    click_on "Edit this cotum", match: :first

    fill_in "Nome", with: @cotum.nome
    click_on "Update Cotum"

    assert_text "Cotum was successfully updated"
    click_on "Back"
  end

  test "should destroy Cotum" do
    visit cotum_url(@cotum)
    click_on "Destroy this cotum", match: :first

    assert_text "Cotum was successfully destroyed"
  end
end
