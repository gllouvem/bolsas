require "application_system_test_case"

class TipoProfessorsTest < ApplicationSystemTestCase
  setup do
    @tipo_professor = tipo_professors(:one)
  end

  test "visiting the index" do
    visit tipo_professors_url
    assert_selector "h1", text: "Tipo professors"
  end

  test "should create tipo professor" do
    visit tipo_professors_url
    click_on "New tipo professor"

    fill_in "Descricao", with: @tipo_professor.descricao
    click_on "Create Tipo professor"

    assert_text "Tipo professor was successfully created"
    click_on "Back"
  end

  test "should update Tipo professor" do
    visit tipo_professor_url(@tipo_professor)
    click_on "Edit this tipo professor", match: :first

    fill_in "Descricao", with: @tipo_professor.descricao
    click_on "Update Tipo professor"

    assert_text "Tipo professor was successfully updated"
    click_on "Back"
  end

  test "should destroy Tipo professor" do
    visit tipo_professor_url(@tipo_professor)
    click_on "Destroy this tipo professor", match: :first

    assert_text "Tipo professor was successfully destroyed"
  end
end
