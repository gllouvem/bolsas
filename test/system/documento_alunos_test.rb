require "application_system_test_case"

class DocumentoAlunosTest < ApplicationSystemTestCase
  setup do
    @documento_aluno = documento_alunos(:one)
  end

  test "visiting the index" do
    visit documento_alunos_url
    assert_selector "h1", text: "Documento alunos"
  end

  test "should create documento aluno" do
    visit documento_alunos_url
    click_on "New documento aluno"

    fill_in "Aluno", with: @documento_aluno.aluno_id
    fill_in "Documento", with: @documento_aluno.documento
    fill_in "Tipo documento", with: @documento_aluno.tipo_documento_id
    click_on "Create Documento aluno"

    assert_text "Documento aluno was successfully created"
    click_on "Back"
  end

  test "should update Documento aluno" do
    visit documento_aluno_url(@documento_aluno)
    click_on "Edit this documento aluno", match: :first

    fill_in "Aluno", with: @documento_aluno.aluno_id
    fill_in "Documento", with: @documento_aluno.documento
    fill_in "Tipo documento", with: @documento_aluno.tipo_documento_id
    click_on "Update Documento aluno"

    assert_text "Documento aluno was successfully updated"
    click_on "Back"
  end

  test "should destroy Documento aluno" do
    visit documento_aluno_url(@documento_aluno)
    click_on "Destroy this documento aluno", match: :first

    assert_text "Documento aluno was successfully destroyed"
  end
end
