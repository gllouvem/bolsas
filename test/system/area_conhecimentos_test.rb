require "application_system_test_case"

class AreaConhecimentosTest < ApplicationSystemTestCase
  setup do
    @area_conhecimento = area_conhecimentos(:one)
  end

  test "visiting the index" do
    visit area_conhecimentos_url
    assert_selector "h1", text: "Area conhecimentos"
  end

  test "should create area conhecimento" do
    visit area_conhecimentos_url
    click_on "New area conhecimento"

    fill_in "Descricao", with: @area_conhecimento.descricao
    click_on "Create Area conhecimento"

    assert_text "Area conhecimento was successfully created"
    click_on "Back"
  end

  test "should update Area conhecimento" do
    visit area_conhecimento_url(@area_conhecimento)
    click_on "Edit this area conhecimento", match: :first

    fill_in "Descricao", with: @area_conhecimento.descricao
    click_on "Update Area conhecimento"

    assert_text "Area conhecimento was successfully updated"
    click_on "Back"
  end

  test "should destroy Area conhecimento" do
    visit area_conhecimento_url(@area_conhecimento)
    click_on "Destroy this area conhecimento", match: :first

    assert_text "Area conhecimento was successfully destroyed"
  end
end
