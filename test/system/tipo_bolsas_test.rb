require "application_system_test_case"

class TipoBolsasTest < ApplicationSystemTestCase
  setup do
    @tipo_bolsa = tipo_bolsas(:one)
  end

  test "visiting the index" do
    visit tipo_bolsas_url
    assert_selector "h1", text: "Tipo bolsas"
  end

  test "should create tipo bolsa" do
    visit tipo_bolsas_url
    click_on "New tipo bolsa"

    fill_in "Nome", with: @tipo_bolsa.nome
    fill_in "Valor", with: @tipo_bolsa.valor
    click_on "Create Tipo bolsa"

    assert_text "Tipo bolsa was successfully created"
    click_on "Back"
  end

  test "should update Tipo bolsa" do
    visit tipo_bolsa_url(@tipo_bolsa)
    click_on "Edit this tipo bolsa", match: :first

    fill_in "Nome", with: @tipo_bolsa.nome
    fill_in "Valor", with: @tipo_bolsa.valor
    click_on "Update Tipo bolsa"

    assert_text "Tipo bolsa was successfully updated"
    click_on "Back"
  end

  test "should destroy Tipo bolsa" do
    visit tipo_bolsa_url(@tipo_bolsa)
    click_on "Destroy this tipo bolsa", match: :first

    assert_text "Tipo bolsa was successfully destroyed"
  end
end
