require "application_system_test_case"

class ProjetoAlunosTest < ApplicationSystemTestCase
  setup do
    @projeto_aluno = projeto_alunos(:one)
  end

  test "visiting the index" do
    visit projeto_alunos_url
    assert_selector "h1", text: "Projeto alunos"
  end

  test "should create projeto aluno" do
    visit projeto_alunos_url
    click_on "New projeto aluno"

    fill_in "Aluno", with: @projeto_aluno.aluno_id
    fill_in "Fim", with: @projeto_aluno.fim
    fill_in "Inicio", with: @projeto_aluno.inicio
    fill_in "Plano trabalho", with: @projeto_aluno.plano_trabalho
    fill_in "Projeto", with: @projeto_aluno.projeto_id
    fill_in "Tipo bolsa", with: @projeto_aluno.tipo_bolsa_id
    click_on "Create Projeto aluno"

    assert_text "Projeto aluno was successfully created"
    click_on "Back"
  end

  test "should update Projeto aluno" do
    visit projeto_aluno_url(@projeto_aluno)
    click_on "Edit this projeto aluno", match: :first

    fill_in "Aluno", with: @projeto_aluno.aluno_id
    fill_in "Fim", with: @projeto_aluno.fim
    fill_in "Inicio", with: @projeto_aluno.inicio
    fill_in "Plano trabalho", with: @projeto_aluno.plano_trabalho
    fill_in "Projeto", with: @projeto_aluno.projeto_id
    fill_in "Tipo bolsa", with: @projeto_aluno.tipo_bolsa_id
    click_on "Update Projeto aluno"

    assert_text "Projeto aluno was successfully updated"
    click_on "Back"
  end

  test "should destroy Projeto aluno" do
    visit projeto_aluno_url(@projeto_aluno)
    click_on "Destroy this projeto aluno", match: :first

    assert_text "Projeto aluno was successfully destroyed"
  end
end
