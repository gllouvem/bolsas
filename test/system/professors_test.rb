require "application_system_test_case"

class ProfessorsTest < ApplicationSystemTestCase
  setup do
    @professor = professors(:one)
  end

  test "visiting the index" do
    visit professors_url
    assert_selector "h1", text: "Professors"
  end

  test "should create professor" do
    visit professors_url
    click_on "New professor"

    fill_in "Cpf", with: @professor.cpf
    fill_in "Laboratorio", with: @professor.laboratorio_id
    fill_in "Matricula", with: @professor.matricula
    fill_in "Nome", with: @professor.nome
    fill_in "Tipo professor", with: @professor.tipo_professor_id
    click_on "Create Professor"

    assert_text "Professor was successfully created"
    click_on "Back"
  end

  test "should update Professor" do
    visit professor_url(@professor)
    click_on "Edit this professor", match: :first

    fill_in "Cpf", with: @professor.cpf
    fill_in "Laboratorio", with: @professor.laboratorio_id
    fill_in "Matricula", with: @professor.matricula
    fill_in "Nome", with: @professor.nome
    fill_in "Tipo professor", with: @professor.tipo_professor_id
    click_on "Update Professor"

    assert_text "Professor was successfully updated"
    click_on "Back"
  end

  test "should destroy Professor" do
    visit professor_url(@professor)
    click_on "Destroy this professor", match: :first

    assert_text "Professor was successfully destroyed"
  end
end
